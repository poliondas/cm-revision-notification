<?php

/**
 * @package CmRevisionNotification
 */
/*
  Plugin Name: Cm Post Revision Notification
  Depends: .
  Plugin URI: https://bitbucket.org/poliondas/cm-post-revision-notification/overview
  Description: Notifica um administrador sobre um post aguardando aprovação.
  Version: 1.0.0
  Author: Carlos Eduardo Tormina Mateus
  Author URI: https://www.linkedin.com/in/carlos-mateus-52605a9a/
  License: GPLv2 or later
  Text Domain: site
 */

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('CMREVISIONNOTIFICATION_PLUGIN_DIR', plugin_dir_path(__FILE__));

register_activation_hook(__FILE__, array('CmRevisionNotification', 'plugin_activation'));
register_deactivation_hook(__FILE__, array('CmRevisionNotification', 'plugin_deactivation'));

require_once( CMREVISIONNOTIFICATION_PLUGIN_DIR . 'class.cmrevisionnotification.php' );

add_action('init', array('CmRevisionNotification', 'init'));
add_action('wp_enqueue_scripts', array('CmRevisionNotification', 'add_script_custom'));
add_action('admin_enqueue_scripts', array('CmRevisionNotification', 'add_admin_script_custom'));
