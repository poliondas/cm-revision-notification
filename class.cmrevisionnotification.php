<?php

class CmRevisionNotification
{

    public static function init()
    {
        # code ...
        add_action('admin_init', array('CmRevisionNotification', 'check_plugin_dependences'));
        add_action('admin_init', array('CmRevisionNotification', 'add_meta_box'));
        add_action('wp_ajax_post_revision_pending_notify', array('CmRevisionNotification', 'post_revision_pending_notify'));
        add_action('wp_ajax_nopriv_post_revision_pending_notify', array('CmRevisionNotification', 'post_revision_pending_notify'));
    }

    public static function plugin_activation()
    {
        # code...
    }

    public static function plugin_deactivation()
    {
        # code ...
    }

    public static function check_plugin_dependences()
    {
        # code ...
    }

    public static function desactivate_plugin()
    {
        # code ...
    }

    public static function add_script_custom()
    {
        //wp_enqueue_script('ajax-notification', WP_PLUGIN_URL . '/cm-revision-notification/js/admin.js', array('jquery'), null, true);
    }

    public static function add_admin_script_custom()
    {
        wp_enqueue_script('revision-notification', WP_PLUGIN_URL . '/cm-revision-notification/js/back.js', array('jquery'), null, true);
        wp_enqueue_style('revision-notification', WP_PLUGIN_URL . '/cm-revision-notification/css/back.css', array(), null, 'all');
    }

    public static function add_meta_box()
    {
        add_meta_box('revision-notification', 'Notificar Revisor', array('CmRevisionNotification', 'revision_notification'), 'post', 'side', 'high');
    }

    public static function revision_notification()
    {
        global $post, $current_screen;
        //print_r($current_screen);\
        $args = array(
            'role' => '',
            'role__in' => array('administrator'),
            'number' => 100,
        );
        $users = get_users($args);
        $aEmail = array();
        foreach ($users as $user) {
            $aEmail[] = "<option value='{$user->user_email}'>({$user->user_login}) {$user->user_email}</option>";
        }
        echo "
            <div class='input-group'>
                <div>
                <select id='notify_email'>" . implode($aEmail) . "</select>
                </div>
                <div>
                    <a class='button btn-notify'>Notificar</a>
                </div>
            </div>
          ";
    }

    public static function post_revision_pending_notify($options = array())
    {
        global $post;
        $data = $_POST['data'];
        $email = (isset($options['email'])) ? (int) $options['email'] : (string) $data['email'];
        $postId = (isset($options['postId'])) ? (int) $options['postId'] : (int) $data['postId'];

        //$email   = 'carlos@marknet.com.br';//get_option( 'admin_email' );
        $post_title = get_the_title($post);
        $subject = '[REVISAR NOVO POST] ' . $post_title;
        $message = 'Existe um novo post para revisão: ' . $post_title . "\n\n";
        $message .= 'Revisar o post: ' . admin_url("post.php?post={$postId}&action=edit");

        wp_mail($email, $subject, $message);
        wp_die();
    }

}
