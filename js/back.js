/* global ajaxurl, postId */
jQuery(function ($) {
    $("#revision-notification .btn-notify").on('click', function () {
        var $btn = $(this);
        $btn.text('Enviando...').attr('disabled', true);
        jQuery.post(
            ajaxurl,
            {
                'action': 'post_revision_pending_notify',
                'data': {
                    email: $('#notify_email').val(),
                    postId: $('#post_ID').val(),
                }
            },
            function (response) {
                $btn.text('Pronto!');
                setTimeout(function(){
                    $btn.text('Enviar').removeAttr('disabled');
                },1000)
            }
        );
    });
});